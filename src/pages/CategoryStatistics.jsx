import { useState, useEffect } from "react";
import ProductInfoTable from "../components/statistics/product/ProductInfoTable.jsx";
import { statisticService } from "../services/statistics.service.jsx";
import ProductStatsChart from "../components/statistics/product/ProductStatsChart.jsx";
import { Container, Row, Col, Card, Form, Button } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import CategoryStatsChart from "../components/statistics/category/CategoryStatsChart.jsx";
import CategoryInfoTable from "../components/statistics/category/CategoryInfoTable.jsx";

const periodToDays = {
    week: 7,
    month: 30,
    year: 365,
};

function CategoryStatistics() {
    const [items, setItems] = useState([]);
    const [shouldRender, setShouldRender] = useState(false);
    const [errorOccurred, setErrorOccurred] = useState(false);
    const [categoryUrl, setCategoryUrl] = useState(null);
    const [period, setPeriod] = useState(7);

    const handleInputChange = (event) => {
        setCategoryUrl(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (categoryUrl) {
            try {
                const result = await statisticService.categoryStatList(categoryUrl, period);
                setItems(result);
                setErrorOccurred(false);
            } catch (error) {
                setErrorOccurred(true);
                console.log(error);
            } finally {
                setShouldRender(true);
            }
        }
    };

    return (
        <Container style={{ padding: 20 }}>
            <Row className="justify-content-center">
                <Col md={5}>
                    <Row>
                        <Form
                            onSubmit={handleSubmit}
                            style={{ display: "flex", justifyContent: "center" }}
                        >
                            <Form.Group controlId="categoryUrl.ControlInput1">
                                <Form.Control
                                    type="text"
                                    placeholder="Category URL"
                                    onChange={handleInputChange}
                                />
                            </Form.Group>
                            <Dropdown style={{ marginLeft: 10 }}>
                                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                    {period === 7
                                        ? "Week"
                                        : period === 30
                                            ? "Month"
                                            : "Year"}
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={() => setPeriod(7)}>
                                        Week
                                    </Dropdown.Item>
                                    <Dropdown.Item onClick={() => setPeriod(30)}>
                                        Month
                                    </Dropdown.Item>
                                    <Dropdown.Item onClick={() => setPeriod(365)}>
                                        Year
                                    </Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                            <Button
                                className="btn btn-primary"
                                type="submit"
                                style={{ marginLeft: 10 }}
                            >
                                Search
                            </Button>
                        </Form>
                    </Row>
                </Col>
            </Row>
            {shouldRender && errorOccurred ? (
                <Row md={4} style={{ justifyContent: "center" }}>
                    <div
                        style={{ margin: 20 }}
                        className="alert alert-danger"
                        role="alert"
                    >
                        <strong>Error!</strong> No info about this category.
                    </div>
                </Row>
            ) : (
                <div>
                    {shouldRender && (
                        <Container style={{ padding: 20 }}>
                            <Row className="mb-4" >
                                <Col>
                                    <Card>
                                        <Container style={{ padding: 20 }}>
                                            <CategoryStatsChart data={items} />
                                        </Container>
                                    </Card>
                                </Col>
                            </Row>
                            <Row className="mb-4">
                                <Col>
                                    <CategoryInfoTable data={items} />
                                </Col>
                            </Row>
                        </Container>
                    )}
                </div>
            )}
        </Container>
    );
}

export default CategoryStatistics;
