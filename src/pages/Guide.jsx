import { Container, Row, Col, Nav, Card } from 'react-bootstrap';

const Guide = () => {
    return (
        <Container style={{padding: 20}}>
            <Row>
                <Col className="mb-4 flex-column text-center">
                    <h1>Statistics Site User Documentation</h1>
                </Col>
            </Row>
            <Row>
                <Col md={3}>
                    <h2>Navigation</h2>
                    <Nav className="flex-column">
                        <Nav.Link href="#product-statistics">Product Statistics</Nav.Link>
                        <Nav.Link href="#category-statistics">Category Statistics</Nav.Link>
                        <Nav.Link href="#dashboard">Dashboard</Nav.Link>
                    </Nav>
                </Col>
                <Col md={8}>
                    <h2 id="product-statistics">Product Statistics</h2>
                    <p>
                        To obtain statistics for a specific product, you need to provide the product SKU. Use the following steps to retrieve the stats:
                    </p>
                    <ol>
                        <li>Go to the <strong>Product statistics</strong> page.</li>
                        <li>Enter the product SKU in the provided input field.</li>
                        <li>Click on the <strong>Search</strong> button.</li>
                        <li>The statistics for the product will be displayed.</li>
                    </ol>

                    <h2 id="category-statistics">Category Statistics</h2>
                    <p>
                        To obtain statistics for a specific category, you need to provide the category URL. Follow the steps below to retrieve the stats:
                    </p>
                    <ol>
                        <li>Go to the <strong>Category statistics</strong> page.</li>
                        <li>Enter the category URL in the provided input field.</li>
                        <li>Click on the <strong>Search</strong> button.</li>
                        <li>The statistics for the category will be displayed.</li>
                    </ol>

                    <h2 id="dashboard">Dashboard</h2>
                    <p>
                        The dashboard page allows you to change your nickname and password. Follow these instructions to make changes:
                    </p>
                    <ol>
                        <li>Click on your avatar in the upper right corner.</li>
                        <li>Click on <strong>Update profile</strong> button.</li>
                        <li>Locate the <strong>Name</strong> and <strong>Password</strong> fields.</li>
                        <li>Enter your desired nickname or/and password in the respective input fields.</li>
                        <li>Click on the <strong>Update</strong> button to apply the changes.</li>
                    </ol>

                    <Card>
                        <Card.Body>
                            <Card.Title>Additional Information</Card.Title>
                            <Card.Text>
                                If you encounter any issues or need further assistance, please contact our support team at support@wbcollect.ru.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};

export default Guide;
