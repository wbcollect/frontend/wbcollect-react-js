import 'bootstrap/dist/css/bootstrap.min.css';

export default function LandingPage() {
    return (
        <div className="container">
            <h1 className="mt-5">Welcome to the Landing Page</h1>
            <p className="lead">This is the landing page content.</p>
        </div>
    );
}