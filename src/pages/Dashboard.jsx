import { useState } from "react";
import {Card, Button, Alert, ListGroup, Row, Col, Container} from "react-bootstrap";
import { useAuth } from "../contexts/AuthContext.jsx";
import { Link, useNavigate } from "react-router-dom";

export default function Dashboard() {
    const [error, setError] = useState("");
    const { currentUser, logout } = useAuth();
    const navigate = useNavigate();

    async function handleLogout() {
        setError("");

        try {
            await logout();
            navigate("/login");
        } catch {
            setError("Failed to log out");
        }
    }

    return (
        <Container style={{padding:20}}>
            <Row className="justify-content-center">
                <Col md={5}>
                    <Card>
                        <Card.Body>
                            <h2 className="text-center mb-4">Profile</h2>
                            {error && <Alert variant="danger">{error}</Alert>}
                            <ListGroup>
                                <ListGroup.Item>
                                    <strong>E-mail:</strong> {currentUser.email}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Username:</strong> {currentUser.username}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Status:</strong> {currentUser.status}
                                </ListGroup.Item>
                            </ListGroup>
                            <Link to="/update-profile" className="btn btn-primary w-100 mt-3">
                                Update Profile
                            </Link>
                        </Card.Body>
                    </Card>
                    <div className="w-100 text-center mt-2">
                        <Button variant="link" onClick={handleLogout}>
                            Log Out
                        </Button>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}
