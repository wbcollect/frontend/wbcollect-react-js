import {useEffect, useState} from "react";
import ProductInfoTable from "../components/statistics/product/ProductInfoTable.jsx";
import {statisticService} from "../services/statistics.service.jsx";
import ProductInfoList from "../components/statistics/product/ProductInfoList.jsx";
import ProductStatsChart from "../components/statistics/product/ProductStatsChart.jsx";
import {Button, Card, Col, Container, Form, Row} from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";

const periodToDays = {
    week: 7,
    month: 30,
    year: 365
};

function ProductStatistics() {
    const [user, setUser] = useState({});
    const [items, setItems] = useState([]);
    const [shouldRender, setShouldRender] = useState(false);
    const [errorOccurred, setErrorOccurred] = useState(false);
    const [id, setId] = useState(null);
    const [period, setPeriod] = useState(7);
    const [product, setProduct] = useState({});

    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem("currentUser")));
    }, []);

    const handleInputChange = (event) => {
        setId(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id) {
            try {
                const result = await statisticService.productStatList(id, period);
                setItems(result);

                const product = await statisticService.productInfo(id);
                setProduct(product);
                setErrorOccurred(false);
            } catch (error) {
                setErrorOccurred(true);
                console.log(error);
            } finally {
                setShouldRender(true);
            }
        }
    };

    if (user.status === "NEW") {
        return (
            <div className="alert alert-danger" role="alert">
                <strong>No access!</strong> You need to have a subscription to use the service.
            </div>
        );
    }

    return (
        <Container style={{padding:20}}>
            <Row className="justify-content-center">
                <Col md={5}>
                    <Row>
                        <Form onSubmit={handleSubmit} style={{display: "flex", justifyContent: "center"}}>
                                <Form.Group controlId="productId.ControlInput1">
                                    <Form.Control type="text" placeholder="Product ID" onChange={handleInputChange}/>
                                </Form.Group>
                            <Dropdown style={{marginLeft: 10}}>
                                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                    {period === 7 ? "Week" : period === 30 ? "Month" : "Year"}
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={() => setPeriod(7)}>Week</Dropdown.Item>
                                    <Dropdown.Item onClick={() => setPeriod(30)}>Month</Dropdown.Item>
                                    <Dropdown.Item onClick={() => setPeriod(365)}>Year</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                            <Button className="btn btn-primary" type="submit" style={{marginLeft: 10}}>
                                Search
                            </Button>
                        </Form>
                    </Row>
                </Col>
            </Row>
            {shouldRender && errorOccurred ? (
                <Row md={4} style={{justifyContent: "center"}}>
                    <div style={{margin: 20}} className="alert alert-danger" role="alert">
                        <strong>Error!</strong> No info about this product.
                    </div>
                </Row>
            ) : (
                <div>
                    {shouldRender && <Container style={{padding:20}}>
                        <Row className="mb-4">
                            <Col md={8}>
                                <Card>
                                    <Container style={{padding:20}}>
                                        <ProductStatsChart data={items}/>
                                    </Container>
                                </Card>
                            </Col>
                            <Col md={4} >
                                <Container className="d-flex" style={{height: "100%"}}>
                                    <ProductInfoList product={product}/>
                                </Container>
                            </Col>
                        </Row>
                        <Row className="mb-4">
                            <Col>
                                <ProductInfoTable data={items}/>
                            </Col>
                        </Row>
                    </Container>}
                </div>
            )
            }
        </Container>
    );
}

export default ProductStatistics;