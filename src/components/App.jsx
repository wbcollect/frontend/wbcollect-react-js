import {Container} from "react-bootstrap";
import {BrowserRouter} from "react-router-dom";
import {AuthProvider} from "../contexts/AuthContext.jsx";
import AuthorizedToolbar from "../headers/AuthorizedToolbar.jsx";
import MyRoutes from "../router/index.jsx";

function App() {
    return (
        <div>
            <AuthProvider>
                <Container
                    style={{minHeight: "100vh"}}
                >
                    <BrowserRouter>
                        <div className="w-100" style={{borderRadius: "50px"}}>
                            <AuthorizedToolbar/>
                        </div>
                        <MyRoutes/>

                    </BrowserRouter>
                </Container>
            </AuthProvider>
        </div>
    );
}

export default App;
