import {ListGroup} from "react-bootstrap";

const ProductInfoList = ({ product }) => {
    return (
            <ListGroup style={{flexDirection: "column"}}>
                <ListGroup.Item variant="primary" className="fw-bold">
                    General Info
                </ListGroup.Item>
                <ListGroup.Item>
                    <strong>SKU:</strong> {product.id}
                </ListGroup.Item>
                <ListGroup.Item>
                    <strong>Name:</strong> {product.name}
                </ListGroup.Item>
                <ListGroup.Item>
                    <strong>Brand:</strong> {product.brandName}
                </ListGroup.Item>
                <ListGroup.Item>
                    <strong>Seller:</strong>
                </ListGroup.Item>
                <ListGroup.Item>
                    <strong>Average rating:</strong>
                </ListGroup.Item>
                <ListGroup.Item>
                    <strong>Last Updated:</strong> {new Date(product.date).toLocaleDateString()}
                </ListGroup.Item>
            </ListGroup>
    );
};

export default ProductInfoList;