import { useTable, useSortBy, usePagination } from "react-table";
import { Table, Pagination, Card, Row, Col } from "react-bootstrap";
import {useMemo} from "react";

export default function ProductInfoTable({ data }) {
    const columns = useMemo(
        () => [
            {
                Header: "Date",
                accessor: "date",
                Cell: ({ value }) => new Date(value).toLocaleDateString("en-GB")
            },
            {
                Header: "Sales",
                accessor: "sales"
            },
            {
                Header: "Quantity",
                accessor: "quantity"
            },
            {
                Header: "Price",
                accessor: "price"
            },
            {
                Header: "Rating",
                accessor: "ratingPercentage"
            }
        ],
        []
    );

    const formattedData = useMemo(() => data.statisticList, [data.statisticList]);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        prepareRow,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state: { pageIndex, pageSize }
    } = useTable(
        {
            columns,
            data: formattedData,
            initialState: { pageIndex: 0, pageSize: 10 }
        },
        useSortBy,
        usePagination
    );

    return (
        <Card>
            <Table striped hover {...getTableProps()} className="mb-0">
                <thead>
                {headerGroups.map((headerGroup) => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map((column) => (
                            <th
                                {...column.getHeaderProps(column.getSortByToggleProps())}
                                className={column.isSorted ? (column.isSortedDesc ? "desc" : "asc") : ""}
                            >
                                {column.render("Header")}
                            </th>
                        ))}
                    </tr>
                ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                {page.map((row) => {
                    prepareRow(row);
                    return (
                        <tr {...row.getRowProps()}>
                            {row.cells.map((cell) => (
                                <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                            ))}
                        </tr>
                    );
                })}
                </tbody>
            </Table>
            <Row className="m-2 d-flex justify-content-center align-items-center">
                <Col md={3} className="d-flex justify-content-center">
                    <Pagination className="m-0">
                        <Pagination.First onClick={() => gotoPage(0)} disabled={!canPreviousPage} />
                        <Pagination.Prev onClick={() => previousPage()} disabled={!canPreviousPage} />
                        {pageOptions.map((page) => (
                            <Pagination.Item
                                key={page}
                                active={pageIndex === page}
                                onClick={() => gotoPage(page)}
                            >
                                {page + 1}
                            </Pagination.Item>
                        ))}
                        <Pagination.Next onClick={() => nextPage()} disabled={!canNextPage} />
                        <Pagination.Last onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage} />
                    </Pagination>
                </Col>
                <Col className="d-flex justify-content-center">
                    <div className="ms-3">
                        Page {pageIndex + 1} of {pageOptions.length}
                    </div>
                </Col>
                <Col md={4}>
                    <Row className="d-flex justify-content-center align-items-center">
                        <Col md={4} className="text-center">
                            Show
                        </Col>
                        <Col sm={4} className="text-center">
                            <select
                                value={pageSize}
                                onChange={(e) => {
                                    setPageSize(Number(e.target.value));
                                }}
                                className="form-select"
                            >
                                {[10, 25, 50].map((size) => (
                                    <option key={size} value={size}>
                                        {size}
                                    </option>
                                ))}
                            </select>
                        </Col>
                        <Col md={4} className="text-center">
                            entries
                        </Col>
                    </Row>
                </Col>
            </Row>

        </Card>
    );
}
