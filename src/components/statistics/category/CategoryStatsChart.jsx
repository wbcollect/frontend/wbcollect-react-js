import React, { useRef, useEffect } from 'react';
import { Chart, CategoryScale, LinearScale, PointElement, LineController, LineElement } from 'chart.js';

Chart.register(CategoryScale, LinearScale, PointElement, LineController, LineElement);

const CategoryStatsChart = ({ data }) => {
    const chartRef = useRef(null);
    const chartInstanceRef = useRef(null);

    useEffect(() => {
        // create new chart when component mounts
        createChart();

        return () => {
            // destroy old chart when component unmounts
            destroyChart();
        };
    }, [data]);

    const destroyChart = () => {
        // check if chart instance exists and destroy it
        if (chartInstanceRef.current) {
            chartInstanceRef.current.destroy();
        }
    };

    const createChart = () => {
        const canvas = chartRef.current; // get canvas element from ref
        const chartData = {
            labels: [],
            datasets: [
                {
                    label: 'Revenue',
                    data: [],
                    backgroundColor: 'rgb(51,121,183)',
                    borderColor: 'rgb(51,121,183)',
                    tension: 0.5,
                    fill: false,
                },
            ],
        };

        if (data && data.statisticList) {
            data.statisticList.forEach((statistic) => {
                chartData.labels.push(new Date(statistic.date).toLocaleDateString());
                chartData.datasets[0].data.push(statistic.revenue);
            });
        }

        // destroy previous chart instance if it exists
        destroyChart();

        // create new chart and store its instance
        chartInstanceRef.current = new Chart(canvas, {
            type: 'line',
            data: chartData,
            options: {
                responsive: true,
                scales: {
                    x: {
                        type: 'category',
                    },
                    y: {
                        beginAtZero: true,
                    },
                },
            },
        });
    };

    return <canvas id="chart-container" ref={chartRef} />;
};

export default CategoryStatsChart;
