import {Navigate, Route, Routes} from "react-router-dom";
import Signup from "../pages/Signup.jsx";
import LoginPage from "../pages/LoginPage.jsx";
import PrivateRoute from "../auth/PrivateRoute.jsx";
import Dashboard from "../pages/Dashboard.jsx";
import UpdateProfile from "../pages/UpdateProfile.jsx";
import ProductStatistics from "../pages/ProductStatistics.jsx";
import Guide from "../pages/Guide.jsx";
import CategoryStatistics from "../pages/CategoryStatistics.jsx";

export default function MyRoutes() {
    return (
        <Routes>
            <Route
                path="*"
                element={<Navigate to="/dashboard" replace />}
            />
            <Route path="/signup" element={<Signup/>}/>
            <Route path="/login" element={<LoginPage/>}/>
            <Route path="/guide" element={<Guide/>}/>
            <Route
                path="/dashboard"
                element={
                    <PrivateRoute>
                        <Dashboard/>
                    </PrivateRoute>
                }
            ></Route>
            <Route
                path="/update-profile"
                element={
                    <PrivateRoute>
                        <UpdateProfile/>
                    </PrivateRoute>
                }
            ></Route>
            <Route path="/category-statistics" element={
                <PrivateRoute>
                    <CategoryStatistics/>
                </PrivateRoute>
            }
            >
            </Route>
            <Route path="/product-statistics" element={
                <PrivateRoute>
                    <ProductStatistics/>
                </PrivateRoute>
            }
            >
            </Route>
        </Routes>
    );
}
