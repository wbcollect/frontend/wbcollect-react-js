import axios from "axios";

export const statisticService = {
    productStatList,
    productInfo,
    categoryStatList,
};

const apiUrl = import.meta.env.VITE_API_URL;

async function categoryStatList(categoryUrl, days) {
    const user = JSON.parse(localStorage.getItem("currentUser"));
    const requestOptions = {
        method: "GET",
        headers: { Authorization: "Basic " + user.authdata },
    };
    const fetchData = async () => {
        const result = await axios.get(
            `${apiUrl}/statistics/category/stat?url=${categoryUrl}&days=${days}`,
            requestOptions
        );
        console.log(result.data);
        return result.data;
    };
    return fetchData();
}

async function productStatList(productId, days) {
    const user = JSON.parse(localStorage.getItem("currentUser"));
    const requestOptions = {
        method: "GET",
        headers: {Authorization: "Basic " + user.authdata},
    };
    const fetchData = async () => {
        const result = await axios.get(
            `${apiUrl}/statistics/product/stat/${productId}?days=${days}`,
            requestOptions
        );
        console.log(result.data);
        return result.data;
    };
    return fetchData();
}

async function productInfo(productId) {
    const user = JSON.parse(localStorage.getItem("currentUser"));
    const requestOptions = {
        method: "GET",
        headers: {Authorization: "Basic " + user.authdata},
    };
    const fetchData = async () => {
        const result = await axios.get(
            `${apiUrl}/statistics/product/info/${productId}`,
            requestOptions
        );
        console.log(result.data);
        return result.data;
    };
    return fetchData();
}

async function handleResponse(response) {
    const data = await response.json();
    if (!response.ok) {
        if (data && data.password) {
            throw new Error(data.password);
        } else if (response.status === 409) {
            throw new Error("Same user already exists!")
        } else {
            throw new Error("Failed to create an account");
        }
    }
    return data;
}
