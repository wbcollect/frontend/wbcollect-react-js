import React, { useState } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Dropdown from 'react-bootstrap/Dropdown';
import Image from 'react-bootstrap/Image';
import { useAuth } from "../contexts/AuthContext.jsx";
import { useNavigate } from "react-router-dom";
import logo from "../assets/images/logo-small.png";
import "./styles/authorized-header.css";

function AuthorizedToolbar() {
    const [error, setError] = useState("");
    const [activeLink, setActiveLink] = useState(null);
    const { currentUser, logout } = useAuth();
    const navigate = useNavigate();

    async function handleLogout() {
        setError("");

        try {
            await logout();
            navigate("/login");
        } catch {
            setError("Failed to log out");
        }
    }

    const handleProfile = () => {
        navigate("/dashboard");
    };

    const handleUpdateProfile = () => {
        navigate("/update-profile");
    }

    const handleProductStatistics = () => {
        setActiveLink('Product statistics');
        navigate("/product-statistics");
    }

    const handleCategoryStatistics = () => {
        setActiveLink('Category statistics');
        navigate("/category-statistics");
    }

    const handleGuide = () => {
        setActiveLink('Guide');
        navigate("/guide");
    }

    if (!currentUser) {
        return null;
    }

    return (
        <Navbar bg="light" expand="lg" className="border-bottom p-3 rounded-bottom">
            <div className="container">
                <Navbar.Brand href="/" className="d-flex align-items-center mb-2 mb-lg-0 link-body-emphasis text-decoration-none">
                    <img src={logo} style={{width: 50}} alt="logo"/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />

                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto mb-2 mb-lg-0 justify-content-center mb-md-0">
                        <Nav.Link
                            key="Product statistics"
                            onClick={handleProductStatistics}
                            className={`nav-link px-4`}
                            active={activeLink === 'Product statistics'}
                        >
                            Product statistics
                        </Nav.Link>
                        <Nav.Link
                            key="Category statistics"
                            onClick={handleCategoryStatistics}
                            className={`nav-link px-4`}
                            active={activeLink === 'Category statistics'}
                        >
                            Category statistics
                        </Nav.Link>
                        <Nav.Link
                            key="Guide"
                            onClick={handleGuide}
                            className={`nav-link px-4`}
                            active={activeLink === 'Guide'}
                        >
                            Guide
                        </Nav.Link>
                    </Nav>

                    <Form className="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3" role="search">
                        <FormControl type="search" placeholder="Search..." aria-label="Search" />
                    </Form>

                    <Dropdown align="end">
                        <Dropdown.Toggle variant="light" id="dropdown-basic" className="d-block link-body-emphasis text-decoration-none custom-toggle-bg">
                            <Image src={`https://eu.ui-avatars.com/api/?name=${currentUser.username}&size=250`} alt="mdo" width="32" height="32" className="rounded-circle" />

                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item onClick={handleProfile}>Profile</Dropdown.Item>
                            <Dropdown.Item onClick={handleUpdateProfile}>Update profile</Dropdown.Item>
                            <Dropdown.Divider></Dropdown.Divider>
                            <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Navbar.Collapse>
            </div>
        </Navbar>
    );
}

export default AuthorizedToolbar;
