import React, { useContext, useState, useEffect } from "react"

const AuthContext = React.createContext({});

const apiUrl = import.meta.env.VITE_API_URL;

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({ children }) {
    const [currentUser, setCurrentUser] = useState()
    const [loading, setLoading] = useState(true)

    function signup(username, email, password) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "username": username,
                "password": password,
                "confirmPassword": password,
                "email": email
            }),
            redirect: 'follow'
        };

        return fetch(`${apiUrl}/users/create`, requestOptions)
            .then(handleResponse)
            .then(user => {
                // login successful if there's a user in the response
                if (user) {
                    // store user details and basic auth credentials in state
                    user.authdata = window.btoa(email + ':' + password);
                    setCurrentUser(user);
                    localStorage.setItem("currentUser", JSON.stringify(user))
                }

                return user;
            });

    }

    function login(email, password) {
        const requestOptions = {
            method: 'GET',
            headers: {'Authorization': 'Basic ' + window.btoa(email + ':' + password)}
        };

        return fetch(`${apiUrl}/users/get-info-by-email/` + email, requestOptions)
            .then(handleResponse)
            .then(user => {
                // login successful if there's a user in the response
                if (user) {
                    // store user details and basic auth credentials in state
                    user.authdata = window.btoa(email + ':' + password);
                    setCurrentUser(user);
                    localStorage.setItem("currentUser", JSON.stringify(user))
                }

                return user;
            });

    }

    function logout() {
        // remove user from state to log user out
        setCurrentUser(null);
        localStorage.removeItem("currentUser")
    }

    function resetPassword(email) {
        console.log("Reset " + email)
    }

    function updateUsername(username) {
        const user = currentUser;
        const authdata = user.authdata;
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Authorization': 'Basic ' + user.authdata,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "newUsername": username,
                "email": user.email
            }),
            redirect: 'follow'
        };
        return fetch(`${apiUrl}/users/change-username`, requestOptions)
            .then(handleResponse)
            .then(user => {
                user.username = username;
                user.authdata = authdata;
                setCurrentUser(user);
                localStorage.setItem("currentUser", JSON.stringify(user));
            })
            .then(() => {
                window.location.reload();
            });
    }

    function updatePassword(oldPassword, newPassword, confirmNewPassword) {
        const user = currentUser;
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Authorization': 'Basic ' + user.authdata,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "oldPassword": oldPassword,
                "password": newPassword,
                "confirmPassword": confirmNewPassword,
                "email": user.email
            }),
            redirect: 'follow'
        };
        return fetch(`${apiUrl}/users/change-password`, requestOptions)
            .then(handleResponse)
            .then(user => {
                // login successful if there's a user in the response
                if (user) {
                    // store user details and basic auth credentials in state
                    user.authdata = window.btoa(user.email + ':' + newPassword);
                    setCurrentUser(user);
                    localStorage.setItem("currentUser", JSON.stringify(user));
                }

                return user;
            })
            .then(() => {
                window.location.reload();
            });
    }

    useEffect(() => {
        const storedUser = localStorage.getItem("currentUser")
        if (storedUser) {
            setCurrentUser(JSON.parse(storedUser))
        }
        setLoading(false)
    }, [])

    const value = {
        currentUser,
        login,
        signup,
        logout,
        resetPassword,
        updateUsername,
        updatePassword
    }

    async function handleResponse(response) {
        const data = await response.json();
        if (!response.ok) {
            if (data && data.password) {
                throw new Error(data.password);
            }
            else if (response.status === 409) {
                throw new Error("Same user already exists!")
            }
            else {
                throw new Error("Failed to create an account");
            }
        }
        return data;
    }

    return (
        <AuthContext.Provider value={value}>
            {!loading && children}
        </AuthContext.Provider>
    )
}
