# Репозиторий Frontend React + Vite

Веб-сайт предоставляет доступ к статистике продаж товаров. Вы можете получить доступ к сайту, используя следующие учетные данные:

**URL сайта:** [https://front-wbcollect-react-js.vercel.app/](https://front-wbcollect-react-js.vercel.app/)

**Логин:** test@test.ru
**Пароль:** Password1

## Использование

Чтобы использовать сайт и получить статистику по товарам или категориям, следуйте инструкциям ниже:

### Статистика товаров

1. Перейдите по URL сайта: [https://front-wbcollect-react-js.vercel.app/](https://front-wbcollect-react-js.vercel.app/).
2. Войдите, используя предоставленные учетные данные (Логин: test@test.ru, Пароль: Password1).
3. В верхнем меню выберите "Product Statistics".
4. Введите идентификатор (ID) товара, для которого вы хотите получить статистику.
   - Например, вы можете ввести один из следующих ID товаров:
     - 87879469
     - 79154892
     - Или любой другой допустимый ID товара из раздела [https://www.wildberries.ru/catalog/knigi-i-kantstovary/kantstovary/karty-i-globusy](https://www.wildberries.ru/catalog/knigi-i-kantstovary/kantstovary/karty-i-globusy)
5. Нажмите клавишу Enter или кликните на кнопку поиска, чтобы просмотреть статистику для указанного товара.

### Статистика категорий

1. Перейдите по URL сайта: [https://front-wbcollect-react-js.vercel.app/](https://front-wbcollect-react-js.vercel.app/).
2. Войдите, используя предоставленные учетные данные (Логин: test@test.ru, Пароль: Password1).
3. В верхнем меню выберите "Category Statistics".
4. Введите URL категории, для которой вы хотите получить статистику.
   - В настоящее время сайт поддерживает отслеживание статистики для категории "/knigi-i-kantstovary/kantstovary/karty-i-globusy".
5. Нажмите клавишу Enter или кликните на кнопку поиска, чтобы просмотреть статистику для указанной категории.

## Используемые технологии

Фронт-энд приложение построено с использованием следующих технологий:

- React: Библиотека JavaScript для создания пользовательских интерфейсов.
- Vite: Сборочный инструмент, который обеспечивает более быструю и эффективную разработку современных веб-проектов.
- Bootstrap 5: CSS-фреймворк, который предоставляет множество стилей и компонентов для создания адаптивного дизайна.
- Chart.js: Библиотека для создания графиков и диаграмм, которые помогают визуализировать данные и сделать интерфейс более наглядным.
